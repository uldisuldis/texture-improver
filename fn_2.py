from time import time_ns

togON  = None
togOFF = None
btnUP  = None
btnDOW = None
tabTOP = None
tabBOT = None
#font = None
#log_font = None
lod_line_even = False
log_line_even = False
last_log_line_count = 0
last_samp_line_count = 0
slid_select = ""
text_select = ""
last_key = ""
last_press = time_ns()

def init_textures():
	global togON, togOFF, btnUP, btnDOW, tabTOP, tabBOT
	from fn_1 import load_img
	togON = load_img("togON")
	togOFF = load_img("togOFF")
	btnUP = load_img("btnUP")
	btnDOW = load_img("btnDOW")
	tabTOP = load_img("tabTOP", x=100)
	tabBOT = load_img("tabBOT", x=100)

def add_btn(name, x, y, w=64, h=32, text="N/A"):
	from fn_1 import BtnData
	from globals import g
	g.button_dic[name] = BtnData(x, y, w, h, text)

def add_tog(name, x, y, on=False):
	from fn_1 import TogData
	from globals import g
	g.toggle_dic[name] = TogData(x, y, 64, 32, on)

def add_sld(name, x, y, w=300, min=0, max=255, value=0, mult=1, offset=0, box_w=69):
	from fn_1 import SldData
	from globals import g
	g.slider_dic[name] = SldData(x, y, w, min, max, value, mult, offset, box_w)

def complete_inputs():
	from globals import g
	for key in g.toggle_dic:
		g.toggle_dic[key].changed = False

def update_input(event):
	global slid_select, text_select, last_key, last_press
	from pygame.locals import MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEWHEEL
	from globals import g
	if event.type == MOUSEBUTTONDOWN and event.button == 1:
		
		for key in g.button_dic:
			btn_data = g.button_dic[key]
			
			r = btn_data.rect.copy()
			if r.x < 0:
				r.x += g.win_wh[0]-64
			if r.y < 0:
				r.y += g.win_wh[1]-32
			if r.collidepoint(event.pos):
				btn_data.on = True
		
		for key in g.toggle_dic:
			tog_data = g.toggle_dic[key]
			
			if tog_data.rect.collidepoint(event.pos):
				tog_data.on = not tog_data.on
				tog_data.changed = True
			
		for key in g.slider_dic:
			slid_data = g.slider_dic[key]
			
			if slid_data.rect.collidepoint(event.pos):
				slid_select = key
		
	elif event.type == MOUSEBUTTONUP and event.button == 1:
		slid_select = ""
	
	if slid_select != "":
		slid_data = g.slider_dic[slid_select]
		try:
			from numpy import clip
			slid_data.frac = clip( (event.pos[0]-slid_data.x-4)/(slid_data.bar_w-0.8), 0,1)
			value = slid_data.frac*(slid_data.max-slid_data.min)+slid_data.min+0.2
			if value < slid_data.min:
				value = slid_data.min
			if value > slid_data.max:
				value = slid_data.max
			slid_data.value = int(int(value)*slid_data.mult*1000)/1000+slid_data.offset
			if slid_data.value == int(slid_data.value):
				slid_data.value = int(slid_data.value)
		except: pass
	
	if event.type == MOUSEWHEEL:
		for key in g.slider_dic:
			slid_data = g.slider_dic[key]
			if slid_data.rect.collidepoint(g.mouse_xy):
				from numpy import clip
				if slid_data.value-slid_data.offset > slid_data.min*slid_data.mult and event.y < 0:
					slid_data.value -= min(1, slid_data.mult)
				if slid_data.value-slid_data.offset < slid_data.max*slid_data.mult and event.y > 0:
					slid_data.value += min(1, slid_data.mult)
				slid_data.value = int((slid_data.value+0.00001)*1000)/1000
				if slid_data.value == int(slid_data.value):
					slid_data.value = int(slid_data.value)
				frac_sld = (slid_data.value-slid_data.offset)//slid_data.mult-slid_data.min
				slid_data.frac = clip(frac_sld/(slid_data.max-slid_data.min), 0,1)

def render_elements():
	from math import copysign
	from fn_1 import draw_bar, draw_surf, draw_text, draw_text_box
	from globals import g
	for key in g.button_dic:
		btn_data = g.button_dic[key]
		
		if btn_data.on:
			draw_surf(btnDOW, btn_data.x, btn_data.y)
			draw_text(btn_data.text, btn_data.x+32, btn_data.y-9+copysign(16, btn_data.y), center=True)
		else:
			draw_surf(btnUP, btn_data.x, btn_data.y)
			draw_text(btn_data.text, btn_data.x+32, btn_data.y-9+copysign(16, btn_data.y), center=True)
	
	for key in g.toggle_dic:
		tog_data = g.toggle_dic[key]
		
		if tog_data.on:
			draw_surf(togON, tog_data.x, tog_data.y)
		else:
			draw_surf(togOFF, tog_data.x, tog_data.y)
	
	for key in g.slider_dic:
		slid_data = g.slider_dic[key]
		draw_bar(slid_data.x, slid_data.y, slid_data.w, slid_data.bar_w, slid_data.frac, slid_data.value)

def draw_log(x, y, w=400, h=200, scroll_y=0, spacing=16, padding=5, border=2):
	global last_log_line_count, log_line_even
	from pygame import Rect as rect
	from fn_0 import create_surf, len2, save_to_clipbord
	from fn_1 import draw_surf
	from globals import g, font, COLOR

	auto_scroll = scroll_y <= -last_log_line_count*spacing or scroll_y >= 0
	
	while len(g.log_line_arr) > 5000:
		g.log_line_arr.pop(0)
		scroll_y += spacing
		log_line_even = not log_line_even
	last_log_line_count = len2(g.log_line_arr)
	
	if auto_scroll:
		scroll_y = -last_log_line_count*spacing
	y_offset = h-border*2+scroll_y
	
	log_surf = create_surf(w, h, COLOR.T6_BLACK if auto_scroll else COLOR.NONE, 2, COLOR.GREY4)
	text_surf = create_surf(w-border*2, h-border*2)
	line_bg_surf_light = create_surf(w-border*2, spacing, COLOR.T4_BLACK)
	line_bg_surf_dark = create_surf(w-border*2, spacing, COLOR.T8_BLACK)
	highlight_surf = create_surf(w-border*2, spacing, COLOR.T6_WHITE)
	
	temp_even = log_line_even
	for line in g.log_line_arr:
		l_h = rect(x, y+border+y_offset, w, spacing*len(line)).collidepoint(g.mouse_xy)
		lines_highlight = l_h and rect(x, y, w, h).collidepoint(g.mouse_xy)
		for line2 in line:
			if y_offset > -spacing and y_offset < h-border:
				if lines_highlight:
					text_surf.blit(highlight_surf, (0, y_offset))
					if g.right_clicked:
						save_to_clipbord("".join(line))
				elif temp_even:
					text_surf.blit(line_bg_surf_light, (0, y_offset))
				else:
					text_surf.blit(line_bg_surf_dark, (0, y_offset))
				text_surf.blit(font.log.render(line2.strip(), True, COLOR.WHITE), (border, y_offset-spacing//8))
			y_offset += spacing
		temp_even = not temp_even
	
	log_surf.blit(text_surf, (border, border))
	draw_surf(log_surf, x, y)
	
	return scroll_y
	
def draw_selectable_list(x, y, w=400, h=200, scroll_y=0, spacing=16, padding=5, border=2):
	global lod_line_even
	from pygame import Rect as rect
	from fn_0 import create_surf, len2, save_to_clipbord
	from fn_1 import draw_surf
	from globals import g, font, COLOR

	if g.current_mode == 0:
		line_arr = g.sampl_line_arr
	if g.current_mode == 1:
		line_arr = g.learn_line_arr
	if g.current_mode == 2:
		line_arr = g.build_line_arr
	
	samp_line_count = len2(line_arr)
	
	if scroll_y < -samp_line_count*spacing:
		scroll_y = -samp_line_count*spacing
	if scroll_y > border-h:
		scroll_y = border-h
	y_offset = h-border+scroll_y
	
	log_surf = create_surf(w, h, COLOR.NONE, 2, COLOR.GREY4)
	text_surf = create_surf(w-border*2, h-border*2)
	line_bg_surf_light = create_surf(w-border*2, spacing, COLOR.T4_BLACK)
	line_bg_surf_dark = create_surf(w-border*2, spacing, COLOR.T8_BLACK)
	highlight_surf = create_surf(w-border*2, spacing, COLOR.T6_WHITE)
	selected_surf = create_surf(w-border*2, spacing, COLOR.T8_WHITE)
	
	samp_chang = False
	temp_even = lod_line_even
	selected_rows = 0
	for line in line_arr:
		l_h = rect(x, y+border+y_offset, w, spacing*len(line)).collidepoint(g.mouse_xy)
		lines_highlight = l_h and rect(x, y, w, h).collidepoint(g.mouse_xy)
		if lines_highlight and g.left_clicked:
			if g.samp_selected != selected_rows:
				g.samp_selected = selected_rows
			else:
				g.samp_selected = -1
			samp_chang = True
		for line2 in line:
			if y_offset > -spacing and y_offset < h-border:
				if g.samp_selected == selected_rows:
					text_surf.blit(selected_surf, (0, y_offset))
					if lines_highlight and g.right_clicked:
						save_to_clipbord("".join(line))
				elif lines_highlight:
					text_surf.blit(highlight_surf, (0, y_offset))
					if g.right_clicked:
						save_to_clipbord("".join(line))
				elif temp_even:
					text_surf.blit(line_bg_surf_light, (0, y_offset))
				else:
					text_surf.blit(line_bg_surf_dark, (0, y_offset))
				text_surf.blit(font.log.render(line2.strip(), True, COLOR.WHITE), (border, y_offset-spacing//8))
			y_offset += spacing
		temp_even = not temp_even
		selected_rows += 1
	
	log_surf.blit(text_surf, (border, border))
	draw_surf(log_surf, x, y)
	
	return scroll_y, samp_chang

def draw_lod_highlight(surf_xy, size, viewer_rect, resizing_samp=False):
	from fn_0 import vec
	from globals import g
	if g.samp_selected >= 0:
		from fn_0 import create_surf, mouse_cursor_resize_rect
		from fn_1 import draw_surf
		samp_obj = g.samp_data_arr[g.samp_selected]
		pos0 = surf_xy+vec(samp_obj.left, samp_obj.top)
		pos1 = vec(*pos0*size, type="i4")

		line_v_w_surf = create_surf(2, samp_obj.height*size+1, "#000000")
		line_v_b_surf = create_surf(2, samp_obj.height*size+1, "#FFFFFF")
		line_h_w_surf = create_surf(samp_obj.width*size+1, 2, "#000000")
		line_h_b_surf = create_surf(samp_obj.width*size+1, 2, "#FFFFFF")
		h = line_v_w_surf.get_height()
		w = line_h_w_surf.get_width()
		
		if time_ns()%4e8 < 2e8:
			draw_surf(line_v_w_surf, *pos1, area=viewer_rect)
			draw_surf(line_h_w_surf, *pos1, area=viewer_rect)
			draw_surf(line_v_w_surf, *pos1+vec(w-2, 0), area=viewer_rect)
			draw_surf(line_h_w_surf, *pos1+vec(0, h-2), area=viewer_rect)
		else:
			draw_surf(line_v_b_surf, *pos1, area=viewer_rect)
			draw_surf(line_h_b_surf, *pos1, area=viewer_rect)
			draw_surf(line_v_b_surf, *pos1+vec(w-2, 0), area=viewer_rect)
			draw_surf(line_h_b_surf, *pos1+vec(0, h-2), area=viewer_rect)
		if resizing_samp: return
		else: return mouse_cursor_resize_rect(g.mouse_xy, pos0, size, viewer_rect, samp_obj)
	return vec(0, 0)

def draw_gui(tab_area, log_area, log_height):
	from fn_0 import create_surf
	from fn_1 import draw_surf, draw_text
	from globals import g
	line_surf = create_surf(tab_area.w, 1, "#000000")
	line2_surf = create_surf(1, g.win_wh[1], "#000000")
	if g.current_mode == 0:
		draw_surf(tabBOT, 3*tab_area.w//6-50, 25, area=tab_area)
		draw_surf(tabBOT, 5*tab_area.w//6-50, 25, area=tab_area)
		draw_surf(line_surf, 0, 56, area=tab_area)
		draw_surf(tabTOP, 1*tab_area.w//6-50, 25, area=tab_area)
	elif g.current_mode == 1:
		draw_surf(tabBOT, 1*tab_area.w//6-50, 25, area=tab_area)
		draw_surf(tabBOT, 5*tab_area.w//6-50, 25, area=tab_area)
		draw_surf(line_surf, 0, 56, area=tab_area)
		draw_surf(tabTOP, 3*tab_area.w//6-50, 25, area=tab_area)
	elif g.current_mode == 2:
		draw_surf(tabBOT, 1*tab_area.w//6-50, 25, area=tab_area)
		draw_surf(tabBOT, 3*tab_area.w//6-50, 25, area=tab_area)
		draw_surf(line_surf, 0, 56, area=tab_area)
		draw_surf(tabTOP, 5*tab_area.w//6-50, 25, area=tab_area)
	if g.current_mode >= 0:
		draw_text("Sample", 1*tab_area.w//6, 31, center=True)
		draw_text("Learn", 3*tab_area.w//6, 31, center=True)
		draw_text("Combine", 5*tab_area.w//6, 31, center=True)
	
	draw_surf(line2_surf, tab_area.w-1, 0, area=tab_area)
	draw_surf(line2_surf, 0, 0, area=log_area)
	draw_surf(line_surf, 0, -log_height-40, area=tab_area)

def update_gui():
	from globals import g
	change = False
	if g.left_clicked:
		from pygame import Rect as rect
		if rect(1*g.menu_width//6-45, 25, 90, 32).collidepoint(g.mouse_xy):
			if g.current_mode != 0:
				change = True
				g.current_mode = 0
		if rect(3*g.menu_width//6-45, 25, 90, 32).collidepoint(g.mouse_xy):
			if g.current_mode != 1:
				change = True
				g.current_mode = 1
		if rect(5*g.menu_width//6-45, 25, 90, 32).collidepoint(g.mouse_xy):
			if g.current_mode != 2:
				change = True
				g.current_mode = 2
	return change