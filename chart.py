
def update_diagram(plot, canvas, line2d, x=None, y=None):
	from numpy import array, append, delete
	
	if x == None:
		line2d.set_xdata(array([], dtype='float64'))
		line2d.set_ydata(array([], dtype='float64'))
	elif len(line2d.get_xdata()) > 10 and line2d.get_xdata()[0] < x-3e10:
		line2d.set_xdata(append(delete(line2d.get_xdata(), 0), x))
		line2d.set_ydata(append(delete(line2d.get_ydata(), 0), y))
	else:
		line2d.set_xdata(append(line2d.get_xdata(), x))
		line2d.set_ydata(append(line2d.get_ydata(), y))
	
	if x != None:
		canvas.axes.set_xlim(xmin=x-3e10, xmax=x)
	
	plot.draw()

def add_diagram(w=500, h=300):
	from psutil import virtual_memory
	from matplotlib import rcParams, use
	from matplotlib.ticker import FuncFormatter
	from fn_1 import fan_format
	from pygame.image import frombuffer
	from pylab import figure
	from numpy import array, sin
	import matplotlib.backends.backend_agg as agg
	from globals import COLOR
	rcParams['font.family'] = 'arial'
	use("Agg")
	
	dpi = 100
	
	fig = figure(figsize=[w/dpi, h/dpi], dpi=dpi, facecolor=COLOR.GREY2)
	canvas = fig.gca()
	xx = array([], dtype='float64')
	line2d, = canvas.plot(xx, sin(xx), color=COLOR.WHITE)
	plot = agg.FigureCanvasAgg(fig)
	surf = frombuffer(plot.get_renderer().buffer_rgba(), plot.get_width_height(), "RGBA")
	
	fig.subplots_adjust(left=64/w, top=196/h, right=394/w, bottom=10.5/h)
	canvas.axes.set_facecolor(COLOR.NONE)
	canvas.xaxis.set_major_formatter(FuncFormatter(fan_format))
	canvas.yaxis.set_major_formatter(FuncFormatter(fan_format))
	canvas.tick_params(axis='x', colors=COLOR.NONE)
	canvas.tick_params(axis='y', colors=COLOR.WHITE)
	canvas.set_xmargin(0.01)
	canvas.set_ymargin(0.01)
	
	canvas.axes.set_ylim(bottom=None, top=None, ymin=0, ymax=virtual_memory()[0]/16**5)
	
	for s in canvas.spines: canvas.spines[s].set_color(COLOR.GREY2)
	
	plot.draw()
	return surf, plot, canvas, line2d

def mainl(log_width, graph_height, input, output):
	from gpuinfo.GPUInfo import gpu_usage
	from time import sleep, time_ns

	diagram, plot, canvas, line2d = add_diagram(log_width, graph_height)
	
	is_on = True
	not_quiting = True
	using_cuda = False
	
	while not_quiting:
		while not input.empty():
			command = input.get_nowait()
			if command[0] == "quit":
				is_on = False
				not_quiting = False
				break
			elif command[0] == "using_cuda":
				using_cuda = command[1]
				if using_cuda:
					from torch.cuda import mem_get_info
					from psutil import virtual_memory
					canvas.axes.set_ylim(ymax=mem_get_info()[1]/16**5)
				else:
					canvas.axes.set_ylim(ymax=virtual_memory()[0]/16**5)
				update_diagram(plot, canvas, line2d)
		
		if is_on:
			from pygame.image import tostring
			if using_cuda:
				update_diagram(plot, canvas, line2d, time_ns(), gpu_usage()[1][0])
			else:
				from psutil import virtual_memory
				update_diagram(plot, canvas, line2d, time_ns(), virtual_memory()[3]/16**5)
			output.put(['chart', tostring(diagram, "RGBA")])
		elif not_quiting:
			sleep(1)
			
	output.put(["done"])

def init_chart_thread(log_width, graph_height):
	from multiprocessing import Process, Queue
	from multiprocessing import freeze_support as py_install_suport
	py_install_suport()
	input = Queue()
	output = Queue()
	thread = Process(target=mainl, args=(log_width, graph_height, input, output), daemon=True)
	thread.start()
	return input, output, thread
