
from nn import init_nn_thread
from chart import init_chart_thread
from time import sleep
import sys
from win import init_win_thread

def cleanup_mei():
	from os.path import join
	from os import listdir
	from shutil import rmtree
	import sys
	
	mei_bundle = getattr(sys, "_MEIPASS", False)

	if mei_bundle:
		dir_mei, current_mei = mei_bundle.split("_MEI")
		for file in listdir(dir_mei):
			if file.startswith("_MEI") and not file.endswith(current_mei):
				try:
					rmtree(join(dir_mei, file))
				except PermissionError:
					pass

def main():
	
	cleanup_mei()
	
	menu_with = 400
	log_width = 400
	graph_height = 200
	
	win_input, win_output, win_thread = init_win_thread(menu_with, log_width, graph_height)
	chart_input, chart_output, chart_thread = init_chart_thread(log_width, graph_height)
	nn_input, nn_output, nn_thread = init_nn_thread()
	
	while True:
		
		#print(win_thread.is_alive())
		#sleep(5)
		while not win_output.empty():
			command = win_output.get_nowait()
			if command[0] == "quit":
				nn_input.put(["quit"])
				chart_input.put(["quit"])
				while nn_output.get() != ["done"] and nn_thread.is_alive():
					sleep(0.1)
				while chart_output.get() != ["done"] and chart_thread.is_alive():
					sleep(0.1)
				while win_output.get() != ["done"] and win_thread.is_alive():
					sleep(0.1)
				if nn_thread.is_alive():
					nn_thread.join()
				if chart_thread.is_alive():
					chart_thread.join()
				if win_thread.is_alive():
					win_thread.join()
				sys.exit()
			elif command[0] == "nn":
				nn_input.put(command[1])
		
		while not nn_output.empty():
			command = nn_output.get_nowait()
			if command[0] == "using_cuda":
				chart_input.put(command)
			win_input.put(command)
		
		while not chart_output.empty():
			command = chart_output.get_nowait()
			win_input.put(command)
			
		if not win_thread.is_alive() or not chart_thread.is_alive() or not nn_thread.is_alive():
			win_output.put(["quit"])

if __name__ == "__main__":
	main()