import torch
from os import environ as os_environ
from torchvision.models import vgg16

torch.cuda.empty_cache()
os_environ['FFMPEG_BINARY'] = 'ffmpeg'

vgg16 = vgg16(pretrained=True).features
ident = torch.tensor([[0.0,0.0,0.0],[0.0,1.0,0.0],[0.0,0.0,0.0]])
sobel_x = torch.tensor([[-1.0,0.0,1.0],[-2.0,0.0,2.0],[-1.0,0.0,1.0]])
lap = torch.tensor([[1.0,2.0,1.0],[2.0,-12,2.0],[1.0,2.0,1.0]])

def run_model(new_img_tensor, nn_model, mask, samp_arr, iter=1, only=-1):
	from torch.nn.functional import interpolate as tens_scale
	with torch.no_grad():
		#img = new_img_tensor[i:i+1]
		#img = new_img_tensor
		if only >= 0:
			nn = nn_model[only]
			for _ in range(iter):
				new_img_tensor[:] = nn(new_img_tensor)
		else:
			img = torch.zeros(*new_img_tensor.shape)
			#div = torch.zeros(*new_img_tensor.shape)
			div = img.clone()
			
			#i = 0
			for nn, m, samp in zip(nn_model, mask, samp_arr):
				if m.sum() == 0: continue
				
				if samp.scale == 1:
					clone = new_img_tensor.clone()
				else:
					clone = tens_scale(new_img_tensor, scale_factor=1/samp.scale, mode="bicubic")
				#clone = new_img_tensor.detach().clone()
				
				for _ in range(iter):
					
					#img[:] = nn(img)
					clone = nn(clone)
				
				if samp.scale == 1:
					img += clone*m*samp.weight
				else:
					img += tens_scale(clone, new_img_tensor.shape[2:], mode="bicubic")*m*samp.weight
				div += m*samp.weight
				#new_img_tensor[i+1] = (new_img_tensor[i+1]+img[0])
				#i += 1
			
			new_img_tensor[:] = img/torch.clamp(div, min=1)

def calc_styles(imgs):
	style_layers = [1, 6, 11, 18, 25]
	mean = torch.tensor([0.485, 0.456, 0.406])[:,None,None]
	std = torch.tensor([0.229, 0.224, 0.225])[:,None,None]
	x = (imgs-mean) / std
	grams = []
	for i, layer in enumerate(vgg16[:max(style_layers)+1]):
		x = layer(x)
		if i in style_layers:
			h, w = x.shape[-2:]
			y = x.clone()  # workaround for pytorch in-place modification bug(?)
			gram = torch.einsum('bchw, bdhw -> bcd', y, y) / (h*w)
			grams.append(gram)
	return grams

def style_loss(grams_x, grams_y):
	loss = 0.0
	for x, y in zip(grams_x, grams_y):
		loss = loss + (x-y).square().mean()
	return loss

def to_nchw(img):
	img = torch.as_tensor(img)
	if len(img.shape) == 3:
		img = img[None,...]
	return img.permute(0, 3, 2, 1)

def np2pil(a):
	from PIL.Image import fromarray
	from numpy import float32, float64
	if a.dtype in [float32, float64]:
		from fn_0 import float_to_byte
		a = float_to_byte(a)
	return fromarray(a)

#!nvidia-smi -L

def load_image(url, crop=None, size=None, mode=None):
	from numpy import float32
	from io import BytesIO
	from requests import get as url_request
	from PIL.Image import open, BICUBIC
	if url.startswith(('http:', 'https:')):
		# wikimedia requires a user agent
		headers = {
			"User-Agent": "Requests in Colab/0.0 (https://colab.research.google.com/; no-reply@google.com) requests/0.0"
		}
		r = url_request(url, headers=headers)
		f = BytesIO(r.content)
	else:
		f = url
	img = open(f)
	
	if crop != None:
		img = img.crop((*crop.topleft, *crop.bottomright))
	if size != None:
		if str(size.__class__) == "<class 'list'>":
			img = img.resize(size, BICUBIC)
		else:
			from math import pow
			w, h = img.size
			area_ratio = pow(size*size/w/h, 0.5)
			w = max(int(w*area_ratio), 16)
			h = max(int(h*area_ratio), 16)
			
			img = img.resize((w, h), BICUBIC)
		
	if mode != None:
		img = img.convert(mode)
	img = float32(img)/255.0
	return img[:,:,:3]#.transpose(1, 0, 2)

def perchannel_conv(x, filters):
	'''filters: [filter_n, h, w]'''
	b, ch, h, w = x.shape
	y = x.reshape(b*ch, 1, h, w)
	y = torch.nn.functional.pad(y, [1, 1, 1, 1], 'circular')
	y = torch.nn.functional.conv2d(y, filters[:,None])
	return y.reshape(b, -1, h, w)

def perception(x):
	filters = torch.stack([ident, sobel_x, sobel_x.T, lap])
	return perchannel_conv(x, filters)

def to_rgb(x):
	return x[...,:3,:,:]+0.5

def save_model(path, samp_obj, ca, pool, style):
	from pickle import dump as pickle_dump
	path += f".{samp_obj.left}.{samp_obj.top}.{samp_obj.width}.{samp_obj.height}"
	with open(path+'.nn', 'wb') as file:
		pickle_dump(ca, file)
	with open(path+'.pool', 'wb') as file:
		pickle_dump(pool, file)
	with open(path+'.style', 'wb') as file:
		pickle_dump(style, file)

def load_model(path, samp_obj):
	from pickle import load as pickle_load
	path += f".{samp_obj.left}.{samp_obj.top}.{samp_obj.width}.{samp_obj.height}"
	with open(path+'.nn', 'rb') as file:
		ca = pickle_load(file)
	with open(path+'.pool', 'rb') as file:
		pool = pickle_load(file)
	with open(path+'.style', 'rb') as file:
		style = pickle_load(file)
	return ca, pool, style

class CellAutoma(torch.nn.Module):
	def __init__(self, layers=9):
		super().__init__()
		self.chn = layers*3
		self.w1 = torch.nn.Conv2d(layers*3*4, layers*24, 1)
		self.w2 = torch.nn.Conv2d(layers*24, layers*3, 1, bias=False)
		self.w2.weight.data.zero_()
		self.loss_log = 0
	
	def forward(self, x, update_rate=0.5):
		y = perception(x)
		y = self.w2(torch.relu(self.w1(y)))
		b, c, h, w = y.shape
		udpate_mask = (torch.rand(b, 1, h, w)+update_rate).floor()
		return x+y*udpate_mask
	
	def seed(self, n, sz=128, szz=None):
		if szz == None:
			szz = sz
		return torch.zeros(n, self.chn, sz, szz)

