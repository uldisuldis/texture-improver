from time import time_ns
from globals import COLOR

surf_cache = {}
time_time = time_ns()
log_send_queue = None

def mei_path(relative_path):
	from os.path import abspath, join
	import sys
	mei_bundle = getattr(sys, "_MEIPASS", False)
	
	if mei_bundle:
		return join(mei_bundle, relative_path)
	return join(abspath("."), relative_path)

def mouse_cursor_resize_rect(mouse_xy, pos, size, viewer_rect, samp_obj):
	from pygame.mouse import set_system_cursor as set_cursor
	
	if viewer_rect.collidepoint(mouse_xy):
		left_in = mouse_xy[0]+2 > pos[0]*size+viewer_rect.x
		right_in = mouse_xy[0]-2 < (pos[0]+samp_obj.width)*size+viewer_rect.x
		up_in = mouse_xy[1]+2 > pos[1]*size+viewer_rect.y
		down_in = mouse_xy[1]-2 < (pos[1]+samp_obj.height)*size+viewer_rect.y
		if left_in and right_in and up_in and down_in:
			left_out = mouse_xy[0]-2 < pos[0]*size+viewer_rect.x
			right_out = mouse_xy[0]+2 > (pos[0]+samp_obj.width)*size+viewer_rect.x
			up_out = mouse_xy[1]-2 < pos[1]*size+viewer_rect.y
			down_out = mouse_xy[1]+2 > (pos[1]+samp_obj.height)*size+viewer_rect.y
			if left_out and not (up_out or down_out):
				set_cursor(7); return vec(-1, 0)
			elif right_out and not (up_out or down_out):
				set_cursor(7); return vec(1, 0)
			elif up_out and not (left_out or right_out):
				set_cursor(8); return vec(0, -1)
			elif down_out and not (left_out or right_out):
				set_cursor(8); return vec(0, 1)
			elif left_out and up_out:
				set_cursor(5); return vec(-1, -1)
			elif right_out and down_out:
				set_cursor(5); return vec(1, 1)
			elif left_out and down_out:
				set_cursor(6); return vec(-1, 1)
			elif right_out and up_out:
				set_cursor(6); return vec(1, -1)
	set_cursor(0); return vec(0, 0)

def split_line_arr(line_arr, w):
	from globals import font
	
	out = []
	for line in line_arr:
		new_line = ''
		new_line_list = []
		for char in line:
			if font.log.size(new_line+char)[0] > w:
				new_line_list.append(new_line)
				#if char == ' ':
				#	new_line = ''
				#else:
				new_line = char
			else:
				new_line += char
		new_line_list.append(new_line)
		out.append(new_line_list)
	return out

def float_to_byte(arr):
	from numpy import uint8, clip
	return uint8(clip(arr, 0, 1)*255)

def add_samp(img, samp_data):
	from globals import g
	
	index = None
	
	if g.samp_selected >= 0:
		
		g.sampled_img_arr.insert(g.samp_selected, img)
		g.pool_img_arr.insert(g.samp_selected, None)#new_ran_surf())
		g.samp_data_arr.insert(g.samp_selected, samp_data)
		
		g.sampl_line_arr.insert(g.samp_selected, "")
		g.learn_line_arr.insert(g.samp_selected, "")
		g.build_line_arr.insert(g.samp_selected, "")
	else:
		index = len(g.samp_data_arr)
		
		g.sampled_img_arr.append(img)
		g.pool_img_arr.append(None)#new_ran_surf())
		g.samp_data_arr.append(samp_data)
		
		g.sampl_line_arr.append("")
		g.learn_line_arr.append("")
		g.build_line_arr.append("")
	
	
	refresh_samp(index=index)

def refresh_samp(all=False, index=None):
	from globals import g
	
	if all:
		r = range(len(g.samp_rect_arr))
	elif index != None:
		r = range(index, index+1)
	else: r = range(1)
	for i in r:
		if not all:
			if index == None:
				i = g.samp_selected
			else: i = index
		
		data = g.samp_data_arr[i]
		
		sampl_arr = f"{data.width}*{data.height} x{data.left} y{data.top}"
		learn_arr = f"{data.w}*{data.h} epoch:{data.epoch} loss:{data.loss}"
		build_arr = f"{data.w}*{data.h} scale:{data.scale} weight:{data.weight}"
		
		g.sampl_line_arr[i] = split_line_arr([sampl_arr], g.menu_width-5*4)[0]
		g.learn_line_arr[i] = split_line_arr([learn_arr], g.menu_width-5*4)[0]
		g.build_line_arr[i] = split_line_arr([build_arr], g.menu_width-5*4)[0]

def del_samp(all=False):
	from globals import g
	if all:
		
		g.samp_data_arr.clear()
		g.sampl_line_arr.clear()
		g.learn_line_arr.clear()
		g.build_line_arr.clear()
		g.sampled_img_arr.clear()
		g.pool_img_arr.clear()
	elif g.samp_selected >= 0:
		
		g.samp_data_arr.pop(g.samp_selected)
		g.sampl_line_arr.pop(g.samp_selected)
		g.learn_line_arr.pop(g.samp_selected)
		g.build_line_arr.pop(g.samp_selected)
		g.sampled_img_arr.pop(g.samp_selected)
		g.pool_img_arr.pop(g.samp_selected)
		g.samp_selected -= 1

def len2(arr):
	total_lenght = 0
	for i in arr:
		total_lenght += len(i)
	return total_lenght

def set_log_queue(queue):
	global log_send_queue
	log_send_queue = queue

def log(text):
	global log_send_queue
	from globals import g
	if log_send_queue == None:
		if str(text.__class__) == "<class 'list'>":
			text_arr = split_line_arr(text, g.log_width-5*4)
			print(*text_arr)
			g.log_line_arr += text_arr
		else:
			text_arr = split_line_arr([str(text)], g.log_width-5*4)
			print(*text_arr)
			g.log_line_arr += text_arr
	else:
		log_send_queue.put(["log", text])

def time(msg="N/A"):
	global time_time
	from time import time_ns
	dif = (time_ns()-time_time)/1e6
	if msg != "Void":
		log(f"{msg}: {dif}ms")
	time_time = time_ns()
	return dif

def vec(*argv, type=None):
	from numpy import array
	return array([*argv], dtype=type)

def get_file(dir=" ", types=[('Img files', ('*.jpg', '*.png'))]):
	from tkinter import Tk, filedialog
	tk_top = Tk()
	tk_top.withdraw()
	tk_top.wm_attributes('-topmost', 1)
	file = filedialog.askopenfilename(parent=tk_top, initialdir=dir, filetypes=types)
	tk_top.destroy()
	return file

def get_dir(dir=" "):
	from tkinter import Tk, filedialog
	tk_top = Tk()
	tk_top.withdraw()
	tk_top.wm_attributes('-topmost', 1)
	directory = filedialog.askdirectory(parent=tk_top, initialdir=dir)
	tk_top.destroy()
	return directory

def export_file(dir=" ", types=[('Img files', ('*.png', '*.jpg'))], ext=[('Img files', ".png", ".jpg")]):
	from tkinter import Tk, filedialog
	tk_top = Tk()
	tk_top.withdraw()
	tk_top.wm_attributes('-topmost', 1)
	file = filedialog.asksaveasfilename(parent=tk_top, initialdir=dir, filetypes=types, defaultextension=ext)
	tk_top.destroy()
	return file

def save_to_clipbord(string):
	from tkinter import Tk
	tk_top = Tk()
	tk_top.withdraw()
	tk_top.clipboard_clear()
	tk_top.clipboard_append(string)
	tk_top.update()
	tk_top.destroy()

def create_surf(w, h, color=COLOR.NONE, border_w=0, border_color=COLOR.WHITE, cache=True):
	import pygame
	if cache:
		global surf_cache
		try:
			return surf_cache[(w, h, color, border_w, border_color)].copy()
		except: pass
	surf = pygame.Surface((w, h), pygame.SRCALPHA)
	surf.fill(color)
	if border_w > 0:
		pygame.draw.rect(surf, border_color, pygame.Rect(0, 0, w, h), border_w+(1 if border_w%2==0 else 0))
	if cache:
		surf_cache[(w, h, color, border_w, border_color)] = surf.copy()
	return surf

def resize_samp(resize_selection, selection_rel_xy, base_samp_wh):
	from globals import g
	samp_obj = g.samp_data_arr[g.samp_selected]
	mouse_pos = g.mouse_xy / 2**g.img_pow
	
	if resize_selection[0] < 0:
		if selection_rel_xy[0]+mouse_pos[0] < 0:
			mouse_pos[0] = -selection_rel_xy[0]
		if selection_rel_xy[0]+mouse_pos[0] > samp_obj.left+samp_obj.width-1:
			mouse_pos[0] = samp_obj.left+samp_obj.width-1-selection_rel_xy[0]
		old_x = samp_obj.left
		samp_obj.left = int(selection_rel_xy[0]+mouse_pos[0])
		samp_obj.width += old_x-samp_obj.left#+0.5
	elif resize_selection[0] > 0:
		if selection_rel_xy[0]+mouse_pos[0] < 1:
			mouse_pos[0] = 1-selection_rel_xy[0]
		if selection_rel_xy[0]+mouse_pos[0] > base_samp_wh[0]-samp_obj.left:
			mouse_pos[0] = base_samp_wh[0]-samp_obj.left-selection_rel_xy[0]
		samp_obj.width = int(selection_rel_xy[0]+mouse_pos[0])
	if resize_selection[1] < 0:
		if selection_rel_xy[1]+mouse_pos[1] < 0:
			mouse_pos[1] = -selection_rel_xy[1]
		if selection_rel_xy[1]+mouse_pos[1] > samp_obj.top+samp_obj.height-1:
			mouse_pos[1] = samp_obj.top+samp_obj.height-1-selection_rel_xy[1]
		old_y = samp_obj.top
		samp_obj.top = int(selection_rel_xy[1]+mouse_pos[1])
		samp_obj.height += old_y-samp_obj.top#+0.5
	elif resize_selection[1] > 0:
		if selection_rel_xy[1]+mouse_pos[1] < 1:
			mouse_pos[1] = 1-selection_rel_xy[1]
		if selection_rel_xy[1]+mouse_pos[1] > base_samp_wh[1]-samp_obj.top:
			mouse_pos[1] = base_samp_wh[1]-samp_obj.top-selection_rel_xy[1]
		samp_obj.height = int(selection_rel_xy[1]+mouse_pos[1])
	
	g.samp_data_arr[g.samp_selected] = samp_obj

def save_metadata(path, metadata):
	from pickle import dump as pickle_dump
	#print("meta", len(metadata[0]), len(metadata[1]), len(metadata[2]))
	with open(path+'.meta', 'wb') as file:
		pickle_dump(metadata, file)

def load_metadata(path):
	from pickle import load as pickle_load
	#print(path+'.meta')
	with open(path+'.meta', 'rb') as file:
		metadata = pickle_load(file)
	return metadata