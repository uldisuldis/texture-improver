from numpy import array
from pygame.font import SysFont
from types import SimpleNamespace as emtyObj

g = emtyObj()
font = emtyObj()
COLOR = emtyObj()

g.win_wh = None
g.left_clicked = False
g.right_clicked = False

g.img_pow = -1
g.current_mode = -1
g.samp_selected = -1

g.mouse_xy = array([0, 0])

g.button_dic = {}
g.toggle_dic = {}
g.slider_dic = {}
g.send_samp_dic = {}

g.log_line_arr = []
g.pool_img_arr = []
g.samp_data_arr = []
g.sampl_line_arr = []
g.learn_line_arr = []
g.build_line_arr = []
g.sampled_img_arr = []

COLOR.BLACK = '#000000'
COLOR.GREY2 = '#222222'
COLOR.GREY4 = '#444444'
COLOR.GREY6 = '#666666'
COLOR.GREY8 = '#888888'
COLOR.GREYa = '#AAAAAA'
COLOR.GREYc = '#CCCCCC'
COLOR.WHITE = '#FFFFFF'

COLOR.NONE = '#00000000'
COLOR.T2_WHITE = '#FFFFFF22'
COLOR.T4_WHITE = '#FFFFFF44'
COLOR.T6_WHITE = '#FFFFFF66'
COLOR.T8_WHITE = '#FFFFFF88'
COLOR.Ta_WHITE = '#FFFFFFAA'
COLOR.Tc_WHITE = '#FFFFFFCC'
COLOR.T2_BLACK = '#00000022'
COLOR.T4_BLACK = '#00000044'
COLOR.T6_BLACK = '#00000066'
COLOR.T8_BLACK = '#00000088'
COLOR.Ta_BLACK = '#000000AA'
COLOR.Tc_BLACK = '#000000CC'

COLOR.MASK_BASE = '#000000FF'
COLOR.MASK = '#FFFFFFFF'

def finish_global_init(menu_width, log_width, window):
	g.menu_width = menu_width
	g.log_width = log_width
	g.window = window
	
	font.txt = SysFont("Ebrima", 17, bold=False, italic=False)
	font.log = SysFont("Courier New", 16, bold=True, italic=False)