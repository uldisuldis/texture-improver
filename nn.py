from msilib.schema import Error
from multiprocessing import cpu_count as get_cpu_thread_num
from time import sleep
import numpy as np
from os import mkdir as create_directory
from os import environ as os_environ
import torch
from torch.utils.checkpoint import checkpoint_sequential
from pygame import Rect as rect
from os.path import exists as path_exists
from traceback import format_exc as get_traceback

from fn_0 import time, set_log_queue, log, float_to_byte, save_metadata, load_metadata

def main(input, output):
	
	set_log_queue(output)
	
	not_quiting = True
	session_lock = ""
	
	while not_quiting:
		
		selec_samp = -1
		learnable_selected = False
		output_pool = False
		using_cuda = False
		iterating = False
		learning = False
		mix = False
		error_hint = ""
		save_name = ""
		save_dir = ""
		samp_arr = []
		sampl_img = []
		sampl_wh = []
		new_img_tensor = []
		mask = []
		nn_layers = 1
		current_mode = -1
		
		ca = None
		opt = None
		pool = None
		log_len = None
		lr_sched = None
		full_img = None
		base_samp_wh = None
		target_style = None
		loss = np.array([9999])
		
		try:
			while not_quiting:
				time("Void")
				
				while not input.empty():
					command = input.get_nowait()
					if command[0] == "quit":
						not_quiting = False
						break
				
					elif command[0] == "new":
						time("Void")
						file_path = command[1]
						using_cuda = command[2]
						nn_layers = command[3]
						
						try:
							if using_cuda:
								torch.set_default_tensor_type('torch.cuda.FloatTensor')
								os_environ['PYTORCH_CUDA_ALLOC_CONF']='max_split_size_mb:32'
							else:
								torch.set_default_tensor_type('torch.FloatTensor')
								torch.set_num_threads(get_cpu_thread_num())
						except:
							using_cuda = False
							log("Failed to enabel CUDA acceleration")
							
						from fn_nn import CellAutoma, style_loss, calc_styles, run_model, load_image
						from fn_nn import to_nchw, np2pil, to_rgb, save_model, load_model
						
						if using_cuda:
							if session_lock == "no CUDA":
								error_hint = "CUDA modification"
								raise Error
							log("CUDA acceleration enabled")
							session_lock = "CUDA"
						else:
							if session_lock == "CUDA":
								error_hint = "CUDA modification"
								raise Error
							log("CUDA acceleration disabled")
							session_lock = "no CUDA"
						
						save_name = file_path.split("/")[-1]
						name_temp = save_name
						i = -1
						if not path_exists("saves"):
							create_directory("saves")
						while path_exists("saves/"+name_temp):
							i += 1
							name_temp = str(i)+"_"+save_name
						save_name = name_temp
						save_dir = "saves/"+save_name
						create_directory(save_dir)
						full_img = load_image(file_path)
						base_samp_wh = full_img.shape[1::-1]
						data = [save_name, base_samp_wh, [], None, None]
						np2pil(full_img).save(save_dir+'/'+save_name)
						save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
						output.put(['set_save_data', *data, float_to_byte(full_img).tobytes()])
						output.put(['using_cuda', using_cuda])
						
						current_mode = 0
						time("Save creation time")
						break
						
					elif command[0] == "load":
						error_hint = "load faile"
						time("Void")
						save_dir = command[1]
						save_name = save_dir.split("/")[-1]
						samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda = load_metadata(save_dir+'/'+save_name)
						
						print(using_cuda)
						
						if using_cuda:
							if session_lock == "no CUDA":
								error_hint = "CUDA modification"
								raise Error
							error_hint = "CUDA faile"
							torch.set_default_tensor_type('torch.cuda.FloatTensor')
							os_environ['PYTORCH_CUDA_ALLOC_CONF']='max_split_size_mb:32'
							error_hint = "load faile"
						else:
							if session_lock == "CUDA":
								error_hint = "CUDA modification"
								raise Error
							torch.set_default_tensor_type('torch.FloatTensor')
							torch.set_num_threads(get_cpu_thread_num())
						
						from fn_nn import CellAutoma, style_loss, calc_styles, run_model, load_image
						from fn_nn import to_nchw, np2pil, to_rgb, save_model, load_model
						
						if using_cuda:
							log("CUDA acceleration enabled")
							session_lock = "CUDA"
						else:
							log("CUDA acceleration disabled")
							session_lock = "no CUDA"
						
						full_img = load_image(save_dir+'/'+save_name)
						base_samp_wh = full_img.shape[1::-1]
						data = [save_name, base_samp_wh, samp_arr, sampl_img, sampl_wh]
						output.put(['set_save_data', *data, float_to_byte(full_img).tobytes(), nn_layers, using_cuda])
						output.put(['using_cuda', using_cuda])
						
						current_mode = 0
						time("Save load time")
						error_hint = ""
						break
						
				while current_mode >= 0:
					
					while not input.empty():
						command = input.get_nowait()
						if command[0] == "quit":
							save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
							iterating = False
							not_quiting = False
							current_mode = -1
							break
						
						elif command[0] == "exit":
							save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
							iterating, learning, output_pool, mix, using_cuda = [False]*5
							samp_arr, sampl_img, sampl_wh, new_img_tensor, mask = [[]]*5
							ca, opt, pool, log_len = [None]*4
							lr_sched, target_style = [None]*2
							current_mode = -1
							selec_samp = -1
							nn_layers = 1
							break
						
						elif command[0] == "save_to_folder":
							path = command[1]
							extra = ""
							i = 1
							from shutil import move as move_folder
							while path_exists(path+'/'+save_name+extra):
								i += 1
								extra = f" ({i})"
							if extra == "":
								move_folder(save_dir, path+'/'+save_name)
								save_dir = path+'/'+save_name
							else:
								create_directory(path+'/'+save_name+extra)
								move_folder(save_dir, path+'/'+save_name+extra+'/'+save_name)
								save_dir = path+'/'+save_name+extra+'/'+save_name
						
						elif command[0] == "set_mode":
							if current_mode == command[1]: continue
							
							iterating, learning, output_pool, mix = [False]*4
							new_img_tensor, mask = [[]]*2
							
							if current_mode == 1:
								if selec_samp >= 0:
									ca.loss_log = log_len
									save_model(save_dir+'/'+save_name, samp_arr[selec_samp], ca, pool, target_style)
							ca, opt, pool, log_len = [None]*4
							lr_sched, target_style = [None]*2
							learnable_selected = False
							
							current_mode = command[1]
							
							if current_mode == 1:
								if selec_samp >= 0:
									ca, pool, target_style = load_model(save_dir+'/'+save_name, samp_arr[selec_samp])
									
									log_len = ca.loss_log
									opt = torch.optim.Adam(ca.parameters(), 1e-3)
									lr_sched = torch.optim.lr_scheduler.MultiStepLR(opt, [max(1e3-log_len, 0)], 1e-1)
							
							save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
							
						elif command[0] == "set_samp_arr":
							samp_arr[command[2]] = command[1]
							
						elif command[0] == "iterate":
							iterating = command[1]
						elif command[0] == "learn":
							learning = command[1]
						elif command[0] == "pool":
							output_pool = command[1]
						elif command[0] == "mix":
							mix = command[1]
							
						elif command[0] == "regen_texture":
							
							time("Void")
							
							samp_rect = rect(command[1].left, command[1].top, command[1].width, command[1].height)
							selec_samp = command[2]
							
							img = load_image(save_dir+'/'+save_name, crop=samp_rect, size=command[3])
							sampl_img[selec_samp] = float_to_byte(img).tobytes()
							sampl_wh[selec_samp] = img.shape[1::-1]
							
							ca = CellAutoma(layers=nn_layers)
							with torch.no_grad():
								pool = ca.seed(256, *img.shape[1::-1])
								target_style = calc_styles(to_nchw(img))
								save_model(save_dir+'/'+save_name, samp_arr[selec_samp], ca, pool, target_style)
								
							log_len = ca.loss_log
							opt = torch.optim.Adam(ca.parameters(), 1e-3)
							lr_sched = torch.optim.lr_scheduler.MultiStepLR(opt, [max(1e3-log_len, 0)], 1e-1)
							
							wh = sampl_wh[selec_samp]
							samp_arr[selec_samp].loss = None
							samp_arr[selec_samp].epoch = log_len
							samp_arr[selec_samp].w = wh[0]
							samp_arr[selec_samp].h = wh[1]
							
							save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
							
							output.put(['regen_sample', samp_arr[selec_samp], sampl_img[selec_samp], wh, selec_samp])
							
							time("Sample creation time")
						
						elif command[0] == "gen_samp_img_pool":
							
							time("Void")
							
							samp_arr = command[1]
							sampl_img = []
							sampl_wh = []
							
							for i in range(len(samp_arr)):
								
								samp_rect = rect(samp_arr[i].left, samp_arr[i].top, samp_arr[i].width, samp_arr[i].height)
								if samp_arr[i].w == None:
									img = load_image(save_dir+'/'+save_name, crop=samp_rect, size=64)
									samp_arr[i].w, samp_arr[i].h = img.shape[1::-1]
								else:
									img = load_image(save_dir+'/'+save_name, crop=samp_rect, size=[samp_arr[i].w, samp_arr[i].h])
								sampl_img.append(float_to_byte(img).tobytes())
								sampl_wh.append(img.shape[1::-1])
								
								dim = f".{samp_rect.x}.{samp_rect.y}.{samp_rect.w}.{samp_rect.h}"
								#print(i, save_dir+'/'+save_name+dim+'.nn')
								if not path_exists(save_dir+'/'+save_name+dim+'.nn'):
									ca_temp = CellAutoma(layers=nn_layers)
									with torch.no_grad():
										pool_temp = ca_temp.seed(256, *img.shape[1::-1])
										target_style_temp = calc_styles(to_nchw(img))
									save_model(save_dir+'/'+save_name, samp_arr[i], ca_temp, pool_temp, target_style_temp)
								
							save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
							
							if current_mode == 1:
								output.put(['set_sample_data', samp_arr, sampl_img, sampl_wh])
								
								for samp in range(len(samp_arr)):
									
									pool_temp = load_model(save_dir+'/'+save_name, samp_arr[samp])[1]
									
									img_wh = pool_temp.shape[2:]
									fold = 1
									while img_wh[0]*fold*2 < img_wh[1]/fold:
										fold *= 2
									while img_wh[0]*fold > img_wh[1]*2/fold:
										fold /= 2
									x = int(16*fold)
									y = 256//x
									
									out = to_rgb(pool_temp).reshape(x,y,3,img_wh[0],img_wh[1]).permute([0, 3, 1, 4, 2])
									output.put(['set_pool_img', out.reshape(img_wh[0]*x,img_wh[1]*y,3).cpu(), samp])
								
								if selec_samp >= 0 and current_mode == 1:
									ca, pool, target_style = load_model(save_dir+'/'+save_name, samp_arr[selec_samp])
									
									log_len = ca.loss_log
									opt = torch.optim.Adam(ca.parameters(), 1e-3)
									lr_sched = torch.optim.lr_scheduler.MultiStepLR(opt, [max(1e3-log_len, 0)], 1e-1)
									learnable_selected = True
									
							time("Sample pool load time")
							
						elif command[0] == "set_sample":
							selec_samp = command[1]
							
						elif command[0] == "change_sample":
							
							time("Void")
							
							if selec_samp != command[1] or not learnable_selected:
								
								if selec_samp >= 0 and learnable_selected:
									ca.loss_log = log_len
									save_model(save_dir+'/'+save_name, samp_arr[selec_samp], ca, pool, target_style)
								
								selec_samp = command[1]
								
								if selec_samp >= 0:
									ca, pool, target_style = load_model(save_dir+'/'+save_name, samp_arr[selec_samp])
									
									log_len = ca.loss_log
									opt = torch.optim.Adam(ca.parameters(), 1e-3)
									lr_sched = torch.optim.lr_scheduler.MultiStepLR(opt, [max(1e3-log_len, 0)], 1e-1)
									learnable_selected = True
								else:
									ca, opt, pool, log_len = [None]*4
									lr_sched, target_style = [None]*2
									learnable_selected = False
								
								time("Model change time")
								save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
						
						elif command[0] == "set_pixels":
							if selec_samp >= 0:
								mask[selec_samp][0] = torch.tensor(command[1])
						
						elif command[0] == "reset_tensor":
							if len(mask) > 0:
								new_img_tensor = torch.zeros(1, nn_layers*3, *mask.shape[:1:-1])
							
						elif command[0] == "new_mask":
							mask = torch.zeros(len(samp_arr), 1, command[1][1], command[1][0], dtype=torch.uint8)
							
							ca = []
							for i in range(len(samp_arr)):
							
								ca.append(load_model(save_dir+'/'+save_name, samp_arr[i])[0])
							
							new_img_tensor = torch.zeros(1, nn_layers*3, *command[1])
					
					if current_mode == 1:
						time("Void")
						if learning and learnable_selected:
							while True:
								with torch.no_grad():
									batch_idx = np.random.choice(len(pool), 4, replace=False)
									x = pool[batch_idx]
									x.requires_grad_(True)
									if log_len%8 == 0:
										x[:1] = ca.seed(1, *pool.shape[2:])
								x = checkpoint_sequential([ca]*np.random.randint(32, 96), 16, x)
								
								imgs = to_rgb(x)
								loss = style_loss(calc_styles(imgs), target_style)+(x-x.clamp(-1.0, 1.0)).abs().sum()
								if loss.isinf().item() == True:
									loss = loss.nan_to_num()
							
								with torch.no_grad():
									loss.backward()
									
								if loss.isinf().item() == True or loss.isnan().item():
									loss = loss.nan_to_num()
									
								#try:
								#	int(loss.item())
								#	break
								#except:
								#	print("X === NaN")
								#	loss = loss.nan_to_num()
								break
									
							with torch.no_grad():
								for p in ca.parameters():
									p.grad /= (p.grad.norm()+1e-7)
								opt.step()
								opt.zero_grad()
								lr_sched.step()
								pool[batch_idx] = x
								log_len += 1
							
							try:
								samp_arr[selec_samp].loss = int(loss.item())
							except:
								print(loss.item())
								#from pickle import dump as pickle_dump
								#with open('C:/Users/soniks109/Desktop/neuT3/saves/loss.item', 'wb') as file:
								#	pickle_dump(loss, file)
								#loss[0] = 999999
								samp_arr[selec_samp].loss = "inf?"
							samp_arr[selec_samp].epoch = log_len
							
							output.put(['update_learn', samp_arr[selec_samp], selec_samp])
							
							time("Epoch time")
							
							if log_len%100 == 0 and selec_samp >= 0:
								ca.loss_log = log_len
								save_model(save_dir+'/'+save_name, samp_arr[selec_samp], ca, pool, target_style)
								time("Model save time")
								save_metadata(save_dir+'/'+save_name, [samp_arr, sampl_img, sampl_wh, nn_layers, using_cuda])
							
							if output_pool:
								time("Void")
								
								img_wh = pool.shape[2:]
								fold = 1
								while img_wh[0]*fold*2 < img_wh[1]/fold:
									fold *= 2
								while img_wh[0]*fold > img_wh[1]*2/fold:
									fold /= 2
								
								x = int(16*fold)
								y = 256//x
								
								out = to_rgb(pool).reshape(x,y,3,img_wh[0],img_wh[1]).permute([0, 3, 1, 4, 2])
								output.put(['set_pool_img', out.reshape(img_wh[0]*x,img_wh[1]*y,3).cpu(), selec_samp])
								
								time("Pool preview output time")
								
					if current_mode == 2:
						if iterating and str(ca.__class__) == "<class 'list'>":
							
							time("Void")
							wait = 0
							
							if mix:
								print("M", mix, selec_samp)
								run_model(new_img_tensor, ca, mask, samp_arr, iter=25)
								wait = time("Image iteration time")
							
							elif selec_samp >= 0:
								print("S", mix, selec_samp)
								run_model(new_img_tensor, ca, mask, samp_arr, iter=25, only=selec_samp)
								wait = time("Image iteration time")
								
							if mix or selec_samp >= 0:
								output.put(['set_comb_img', (new_img_tensor[0,:3].permute([1, 2, 0])+0.5).cpu()])
								
								wait += time("Combined image output time")
								if wait < 200:
									sleep((200-wait)/1000)
							else:
								sleep(0.1)
		except Exception as e:
			
			try:
				if error_hint == "CUDA faile":
					log("––––––––––––––LOAD–ERROR––––––––––––––")
					log("Save reqiers CUDA, but it could not be loaded.")
					log("––––––––––––––––––––––––––––––––––––––")
					log("Save failed to loaded")
				elif error_hint == "load faile":
					log("––––––––––––––LOAD–ERROR––––––––––––––")
					log("Save was invalid or corupt.")
					log("––––––––––––––––––––––––––––––––––––––")
					log("Save failed to loaded")
					
					
				elif error_hint == "CUDA modification":
					log("––––––––––––––LOAD–ERROR––––––––––––––")
					log("Application requires restart to load CUDA saves after non CUDA saves and vice versa.")
					log("––––––––––––––––––––––––––––––––––––––")
					log("Save failed to loaded")
					
					
				elif str(e).find("CUDA out of memory. ") >= 0:
					log("–––––––––––––MEMORY–ERROR–––––––––––––")
					log("System ran out of video memory.")
					log("––––––––––––––––––––––––––––––––––––––")
					log("Save was forcibly closed")
				elif str(e).find("not enough memory: ") >= 0:
					log("–––––––––––––MEMORY–ERROR–––––––––––––")
					log("System ran out of memory.")
					log("––––––––––––––––––––––––––––––––––––––")
					log("Save was forcibly closed")
				else:
					error = get_traceback()
					l = len(error)
					trace_index = l-error[::-1].find("\n", l-error.find("Error:"))
					
					log("––––––––––––UNKNOWN–ERROR–––––––––––––")
					log(error[trace_index:].strip().split("\n"))
					log("––––––––––––––––––––––––––––––––––––––")
					log("–––––––––––––––TRACEBACK––––––––––––––")
					log(error[:trace_index].strip().split("\n"))
					log("––––––––––––––––––––––––––––––––––––––")
					log("Save was forcibly closed")
			
			except:
				log("–ERROR–COULD–NOT–BE–HANDLED–INFO–DUMP–")
				log(get_traceback())
				log("––––––––––––––––––––––––––––––––––––––")
				log("Save was forcibly closed")
			
			output.put(['close_save'])
		
	output.put(["done"])


def init_nn_thread():
	from multiprocessing import Process, Queue
	from multiprocessing import freeze_support as py_install_support
	py_install_support()
	input = Queue()
	output = Queue()
	nn_thread = Process(target=main, args=(input, output), daemon=True)
	nn_thread.start()
	return input, output, nn_thread