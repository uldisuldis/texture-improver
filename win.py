
from numpy import array
from time import time_ns
from pygame import init as win_init
from pygame import quit as win_quit
from pygame import Rect as rect
from pygame import Surface as blank_surf
from pygame import display as win_display
from pygame.time import Clock as init_clock
from pygame.event import get as get_input_events
from pygame.mouse import get_pos as get_mouse_xy
from pygame.image import fromstring as bytes_to_surf
from pygame.image import load as load_icon
from pygame.locals import *
from random import random
from fn_0 import mei_path, get_file, get_dir, log, set_log_queue, vec, add_samp, del_samp, refresh_samp, resize_samp, export_file
from fn_1 import draw_text, draw_surf, del_pixel, surf_from_floats, set_pixel
from fn_2 import init_textures, add_tog, update_input, render_elements, draw_gui, update_gui, complete_inputs
from fn_2 import add_sld, add_btn, draw_log, draw_selectable_list, draw_lod_highlight

def mainn(menu_width, log_width, graph_height, input, output):
	from globals import finish_global_init, g, COLOR
	
	comb_img = None
	img_name = None
	full_img = None
	vram_chart = None
	mode_changed = None
	
	not_quit = True
	painting = False
	eraseing = False
	using_cuda = False
	resizing_samp = False
	viewer_draging = False
	samp_img_pool_loaded = False
	
	log_scroll_y = 0
	samp_update_time = 0
	lod_list_scroll_y = 0
	
	mask = []
	frame_time_arr = []
	
	prev_xy = vec(0, 0)
	resize_selection = vec(0, 0)
	base_samp_wh = vec(1024, 1024)
	viewer_xy = vec(32, 32, type='f8')
	selection_rel_xy = vec(0, 0, type='f8')
	
	clock = init_clock()
	
	win_init()
	win_display.set_caption('Texture Enchancer')
	win_display.set_icon(load_icon(mei_path(f'assets/icon{int(random()*5)+1}.png')))
	window = win_display.set_mode((1800, 900), RESIZABLE)
	
	finish_global_init(menu_width, log_width, window)
	init_textures()
	
	while not_quit:
		
		frame_time_arr.append(clock.get_time())
		if len(frame_time_arr) > 100:
			frame_time_arr.pop(0)
		
		while not input.empty():
			command = input.get_nowait()
			
			if command[0] == 'regen_sample':
				g.samp_data_arr[command[4]] = command[1]
				g.sampled_img_arr[command[4]] = bytes_to_surf(command[2], command[3], "RGB")
				refresh_samp(index=command[4])
			
			elif command[0] == 'set_sample_data':
				g.sampled_img_arr.clear()
				g.samp_data_arr = command[1]
				for i in range(len(g.samp_data_arr)):
					data_plus_wh = (command[2][i], command[3][i])
					g.sampled_img_arr.append(bytes_to_surf(*data_plus_wh, "RGB"))
				refresh_samp()
			
			elif command[0] == 'close_save':
				g.current_mode = -1
				log_scroll_y, samp_update_time, lod_list_scroll_y = [0]*3
				mode_changed, comb_img, full_img = [None]*3
				samp_img_pool_loaded = False
				mask = []
			
			elif command[0] == 'using_cuda':
				using_cuda = command[1]
			
			elif command[0] == 'set_pool_img':
				g.pool_img_arr[command[2]] = surf_from_floats(command[1])
			
			elif command[0] == 'set_comb_img':
				comb_img = surf_from_floats(command[1])
			
			elif command[0] == 'log':
				log(command[1])
			
			elif command[0] == 'update_learn':
				g.samp_data_arr[command[2]] = command[1]
				refresh_samp(index=command[2])
				
				if g.toggle_dic.get('auto_run') != None and g.toggle_dic['auto_run'].on and g.toggle_dic['learn'].on:
					if g.samp_selected >= 0 and g.samp_selected == command[2]:
						if g.samp_data_arr[g.samp_selected].epoch % g.slider_dic["auto_target"].value == 0:
							g.samp_selected += 1
							if g.samp_selected >= len(g.samp_data_arr):
								g.samp_selected = 0
							output.put(["nn", ["change_sample", g.samp_selected]])
			
			elif command[0] == 'set_save_data':
				img_name = command[1]
				base_samp_wh = command[2]
				del_samp(all=True)
				for i in range(len(command[3])):
					if command[4] == None or command[5] == None:
						img = None
					else:
						try:
							img = bytes_to_surf(command[4][i], command[5][i], "RGB")
						except:
							img = None
					add_samp(img, command[3][i])
				
				full_img = bytes_to_surf(command[6], base_samp_wh, "RGB")
				
			elif command[0] == 'chart':
				vram_chart = bytes_to_surf(command[1], (g.log_width, graph_height), "RGBA")
		
		g.mouse_xy = vec(*get_mouse_xy())
		if g.win_wh == None or g.win_wh != window.get_size():
			g.win_wh = window.get_size()
			viewer_rect = rect(g.menu_width, 0, g.win_wh[0]-g.menu_width-g.log_width, g.win_wh[1])
		window.fill(COLOR.GREY2)
		viewer_mouse_xy = (g.mouse_xy-viewer_rect.topleft)
		texture_mouse_xy = viewer_mouse_xy/2**g.img_pow-viewer_xy
		
		for event in get_input_events():
			update_input(event)
			if event.type == QUIT:
				not_quit = False
				if g.samp_selected >= 0:
					output.put(["nn", ["change_sample", -1]])
				output.put(["quit"])
			if event.type == MOUSEWHEEL:
				if viewer_rect.collidepoint(g.mouse_xy):
					if (g.img_pow > -6 or event.y > 0) and (g.img_pow < 9 or event.y < 0):
						g.img_pow = int(g.img_pow*10+event.y)/10
					viewer_xy = viewer_mouse_xy/2**g.img_pow-texture_mouse_xy
				if rect(g.win_wh[0]-g.log_width+5, 5, g.log_width-10, g.win_wh[1]-graph_height-20-10).collidepoint(g.mouse_xy):
					log_scroll_y += event.y*25
				if g.current_mode == 0:
					if rect(5, 105, g.menu_width-10, graph_height).collidepoint(g.mouse_xy):
						lod_list_scroll_y += event.y*25
				elif g.current_mode == 1:
					if rect(5, 145, g.menu_width-10, graph_height).collidepoint(g.mouse_xy):
						lod_list_scroll_y += event.y*25
				elif g.current_mode == 2:
					if rect(5, 225, g.menu_width-10, graph_height).collidepoint(g.mouse_xy):
						lod_list_scroll_y += event.y*25
			
			if event.type == MOUSEBUTTONDOWN:
				if event.button == 1:
					g.left_clicked = True
					if viewer_rect.collidepoint(event.pos) and not painting:
						painting = True
					if resize_selection[0] != 0 or resize_selection[1] != 0 and not resizing_samp and g.current_mode == 0:
						resizing_samp = True
						samp_obj = g.samp_data_arr[g.samp_selected]
						pos = vec(*event.pos) / 2**g.img_pow
						if resize_selection[0] < 0:
							selection_rel_xy[0] = samp_obj.left-pos[0]
						elif resize_selection[0] > 0:
							selection_rel_xy[0] = samp_obj.width-pos[0]
						if resize_selection[1] < 0:
							selection_rel_xy[1] = samp_obj.top-pos[1]
						elif resize_selection[1] > 0:
							selection_rel_xy[1] = samp_obj.height-pos[1]
				elif event.button == 2:
					if rect(g.win_wh[0]-g.log_width+5, 5, g.log_width-10, g.win_wh[1]-graph_height-20-10).collidepoint(g.mouse_xy):
						log_scroll_y = 0
					if not viewer_draging and viewer_rect.collidepoint(event.pos):
						viewer_draging = True
				elif event.button == 3:
					g.right_clicked = True
					if viewer_rect.collidepoint(event.pos) and not eraseing:
						eraseing = True
			if event.type == MOUSEBUTTONUP:
				if event.button == 1:
					if painting:
						painting = False
					if resizing_samp:
						resizing_samp = False
				elif event.button == 2 and viewer_draging:
					viewer_draging = False
				elif event.button == 3 and eraseing:
					eraseing = False
		
		if viewer_draging:
			viewer_xy += (g.mouse_xy-prev_xy) / 2**g.img_pow
		
		if mode_changed == None:
			mode_changed = True
		else:
			if g.current_mode >= 0 and len(g.samp_data_arr) > 0:
				mode_changed = update_gui()
		
		if mode_changed:
			g.button_dic.clear()
			g.toggle_dic.clear()
			g.slider_dic.clear()
			mask = []
			comb_img = None
			
			output.put(["nn", ["set_mode", g.current_mode]])
			viewer_xy = vec(32, 32, type='f8')
			
			if g.current_mode == -1:
				
				add_btn("quit", 5, 30, text="Quit")
				add_btn("new", 5+64+5, 30, text="New")
				add_btn("load", 5+64+5+64+5, 30, text="Load")
				add_tog("cuda", g.menu_width-5-64, 30)
				add_sld("nn_layers", 5, 90, g.menu_width-10, 1, 16, 4)
				
				g.toggle_dic['cuda'].on = True
				g.img_pow = 0
				
			if g.current_mode == 0:
				
				add_btn("exit", 1*g.menu_width//8-32, 65, text="Exit")
				add_btn("save", 3*g.menu_width//8-32, 65, text="Save")
				#add_btn("new", 1*g.menu_width//8-32, 65, text="New")
				#add_btn("load", 3*g.menu_width//8-32, 65, text="Load")
				add_btn("add_samp", 5*g.menu_width//8-32, 65, text="Add")
				add_btn("delete", 7*g.menu_width//8-32, 65, text="Remove")
				
				g.img_pow = -1
			
			elif g.current_mode == 1:
				
				add_tog("learn", 54, 65)
				add_tog("pool", g.menu_width-5-64, 65)
				
				add_sld("resol_sld", 5, 105, g.menu_width-10-64-8, 1, 250, 16, 4)
				add_btn("regen_texture", g.menu_width-5-64, 105, text="Create")
				
				add_tog("auto_run", 5, graph_height+8+165)
				add_sld("auto_target", 5+64+8, graph_height+8+165, g.menu_width-10-64-8, 1, 250, 10, 10)
				
				if not samp_img_pool_loaded:
					output.put(["nn", ["gen_samp_img_pool", g.samp_data_arr]])
					samp_img_pool_loaded = True
					
				if g.samp_selected >= 0:
					output.put(["nn", ["change_sample", g.samp_selected]])
					
				g.img_pow = 1
				
			elif g.current_mode == 2:
				
				add_tog("mix", 5+34, 65)
				add_tog("show_mask", g.menu_width-5-64, 65)
				
				add_sld("comb_w", 5, 105, g.menu_width-10, 1, 322, 16, 16)
				add_sld("comb_h", 5, 145, g.menu_width-10, 1, 322, 16, 16)
				
				add_btn("set_comb_wh", 5, 185, text="New")
				add_btn("reset_tensor", 75, 185, text="Regen")
				add_tog("iterate", g.menu_width-5-64, 185)
				
				add_sld("brush_size", 5, graph_height+8+245, g.menu_width-10, 0, 321, 8, 2, 1)
				
				add_sld("brush_target", 5, graph_height+8+305, g.menu_width-10, 1, 255, 255)
				
				add_sld("brush_hardness", 5, graph_height+8+365, g.menu_width-10, 0, 250, 250, 0.004)
				
				add_sld("samp_scale", 5, graph_height+8+425, g.menu_width-10, 5, 326, 50, 0.02)
				
				add_sld("samp_weight", 5, graph_height+8+485, g.menu_width-10, 1, 322, 50, 0.02)
				
				add_btn("export", 5, graph_height+8+525, text="Export")
				
				if g.samp_selected >= 0:
					from numpy import clip
					g.slider_dic["samp_scale"].value = g.samp_data_arr[g.samp_selected].scale
					scale_sld = g.slider_dic["samp_scale"]
					frac_sld = scale_sld.value//scale_sld.mult-scale_sld.min
					g.slider_dic["samp_scale"].frac = clip(frac_sld/(scale_sld.max-scale_sld.min), 0,1)
					
					g.slider_dic["samp_weight"].value = g.samp_data_arr[g.samp_selected].weight
					weight_sld = g.slider_dic["samp_weight"]
					frac_sld = weight_sld.value//weight_sld.mult-weight_sld.min
					g.slider_dic["samp_weight"].frac = clip(frac_sld/(weight_sld.max-weight_sld.min), 0,1)
				
				#g.toggle_dic['mix'].on = True
				g.toggle_dic['show_mask'].on = True
				g.img_pow = 1
				output.put(["nn", ["mix", g.toggle_dic['mix'].on]])
					
			mode_changed = False
		
		#if g.current_mode >= 0:
		draw_gui(rect(0,0, g.menu_width, g.win_wh[1]), rect(g.win_wh[0]-g.log_width,0, g.log_width, g.win_wh[1]), graph_height)
		
		render_elements()
		draw_text(f'Frame Time: {str(array(frame_time_arr).mean())[:6]}ms', 4, 2)
		extra_zoom = (g.toggle_dic.get('pool') != None and g.toggle_dic['pool'].on)*2
		draw_text(f"Zoom: {int(2**(g.img_pow+extra_zoom)*100)}%", g.menu_width-118, 2)
		
		log_scroll_y = draw_log(g.win_wh[0]-g.log_width+5, 5, g.log_width-10, g.win_wh[1]-graph_height-10-20, scroll_y=log_scroll_y)
		if using_cuda:
			draw_text("VRAM utilization:", g.win_wh[0]-g.log_width+5, g.win_wh[1]-graph_height-21)
		else:
			draw_text("RAM utilization:", g.win_wh[0]-g.log_width+5, g.win_wh[1]-graph_height-21)
		
		if vram_chart != None:
			draw_surf(vram_chart, 1, g.win_wh[1]-graph_height, area=rect(g.win_wh[0]-g.log_width, 0, g.log_width, g.win_wh[1]))
		
		if g.current_mode == -1:
			draw_text("Enable CUDA:", g.menu_width-64-10-105, 30+6)
			draw_text("Network layers:", 4, 70)
		
		if g.current_mode == 0:
			lod_list_scroll_y, _ = draw_selectable_list(5, 105, g.menu_width-10, graph_height, lod_list_scroll_y)
			
			draw_surf(full_img, *viewer_xy, 2**g.img_pow, area=viewer_rect)
			temp_resize_selection = draw_lod_highlight(viewer_xy, 2**g.img_pow, viewer_rect, resizing_samp)
			if str(temp_resize_selection.__class__) == "<class 'numpy.ndarray'>":
				resize_selection = temp_resize_selection
		
		if g.current_mode == 1:
			draw_text("Learn:", 4, 65+6)
			draw_text("Show Pool:", g.menu_width-64-10-83, 65+6)
			
			lod_list_scroll_y, samp_change = draw_selectable_list(5, 145, g.menu_width-10, graph_height, lod_list_scroll_y)
			
			draw_text("Auto", 19, graph_height+8+145)
			draw_text("Sample change interval", g.menu_width-64-10-108, graph_height+8+145)
			
			if samp_change == True:
				output.put(["nn", ["change_sample", g.samp_selected]])
			
			if g.samp_selected >= 0:
				if g.toggle_dic['pool'].on:
					draw_surf(g.pool_img_arr[g.samp_selected], *viewer_xy, 2**(g.img_pow), area=viewer_rect)
				else:
					draw_surf(g.sampled_img_arr[g.samp_selected], *viewer_xy/(2**extra_zoom), 2**(g.img_pow+extra_zoom), area=viewer_rect)
		
		if g.current_mode == 2:
			
			if g.samp_selected >= 0 and len(mask) == len(g.samp_data_arr):
				if painting: set_pixel(texture_mouse_xy, mask)
				if eraseing: del_pixel(texture_mouse_xy, mask)
			
			from pygame.surfarray import array3d as surf_to_array
			for samp in list(g.send_samp_dic.keys()):
				if g.send_samp_dic[samp] < time_ns():
					output.put(["nn", ["set_pixels", surf_to_array(mask[samp])[:,:,0]]])
					del g.send_samp_dic[samp]
			
			
			lod_list_scroll_y, samp_change = draw_selectable_list(5, 225, g.menu_width-10, graph_height, lod_list_scroll_y)
			
			if samp_change == True:
				
				if g.samp_selected >= 0:
					from numpy import clip
					g.slider_dic["samp_scale"].value = g.samp_data_arr[g.samp_selected].scale
					scale_sld = g.slider_dic["samp_scale"]
					frac_sld = scale_sld.value//scale_sld.mult-scale_sld.min
					g.slider_dic["samp_scale"].frac = clip(frac_sld/(scale_sld.max-scale_sld.min), 0,1)
					
					g.slider_dic["samp_weight"].value = g.samp_data_arr[g.samp_selected].weight
					weight_sld = g.slider_dic["samp_weight"]
					frac_sld = weight_sld.value//weight_sld.mult-weight_sld.min
					g.slider_dic["samp_weight"].frac = clip(frac_sld/(weight_sld.max-weight_sld.min), 0,1)
					
				output.put(["nn", ["set_sample", g.samp_selected]])
			
			if g.samp_selected >= 0:
				
				if g.samp_data_arr[g.samp_selected].scale != g.slider_dic["samp_scale"].value:
					g.samp_data_arr[g.samp_selected].scale = g.slider_dic["samp_scale"].value
					if samp_update_time == 0: samp_update_time = time_ns() + 1e8
					
				if g.samp_data_arr[g.samp_selected].weight != g.slider_dic["samp_weight"].value:
					g.samp_data_arr[g.samp_selected].weight = g.slider_dic["samp_weight"].value
					if samp_update_time == 0: samp_update_time = time_ns() + 1e8
				
				if samp_update_time > 0 and samp_update_time < time_ns():
					output.put(["nn", ["set_samp_arr", g.samp_data_arr[g.samp_selected], g.samp_selected]])
					refresh_samp()
					samp_update_time = 0
			
			draw_text("Mix:", 4, 65+6)
			draw_text("Mask:", g.menu_width-64-10-42, 65+6)
			
			draw_text("Iterate:", g.menu_width-64-10-53, 185+6)
			
			draw_text("Brush Size:", 4, graph_height+8+225)
			
			draw_text("Brush Target:", 4, graph_height+8+285)
			
			draw_text("Brush Hardness:", 4, graph_height+8+345)
			
			draw_text("Sample Size:", 4, graph_height+8+405)
			
			draw_text("Sample Weight:", 4, graph_height+8+465)
			
			draw_surf(comb_img, *viewer_xy, 2**g.img_pow, area=viewer_rect)
			
			if g.samp_selected >= 0 and g.toggle_dic['show_mask'].on and g.samp_selected < len(mask):
				draw_surf(mask[g.samp_selected], *viewer_xy, 2**g.img_pow, area=viewer_rect)
		
		if resizing_samp:
			resize_samp(resize_selection, selection_rel_xy, base_samp_wh)
			refresh_samp()
			samp_img_pool_loaded = False
		
		win_display.update()
		clock.tick(5000)
		
		if g.button_dic.get('new') != None and g.button_dic['new'].pressed():
			new_path = get_file()
			if len(new_path) != 0:
				g.samp_selected = -1
				g.current_mode = 0
				mode_changed = None
				print(new_path)
				output.put(["nn", ["new", new_path, g.toggle_dic['cuda'].on, g.slider_dic["nn_layers"].value]])
		
		if g.button_dic.get('load') != None and g.button_dic['load'].pressed():
			new_directory = get_dir()
			if len(new_directory) != 0:
				g.samp_selected = -1
				g.current_mode = 0
				mode_changed = None
				print(new_directory)
				output.put(["nn", ["load", new_directory]])
		
		if g.button_dic.get('export') != None and g.button_dic['export'].pressed() and comb_img != None:
			save_path = export_file()
			if len(save_path) != 0:
				if save_path[-4:] == '.png' or save_path[-4:] == '.png':
					from pygame.image import save as save_surf
					save_surf(comb_img, save_path)
				else:
					log("Unsupported file type")
		
		if g.button_dic.get('quit') != None and g.button_dic['quit'].pressed():
			not_quit = False
			output.put(["quit"])
		
		if g.button_dic.get('exit') != None and g.button_dic['exit'].pressed():
			if not samp_img_pool_loaded:
				output.put(["nn", ["gen_samp_img_pool", g.samp_data_arr]])
			output.put(["exit"])
			input.put(["close_save"])
			
		if g.button_dic.get('save') != None and g.button_dic['save'].pressed():
			save_path = get_dir()
			if len(save_path) != 0:
				if not samp_img_pool_loaded:
					output.put(["nn", ["gen_samp_img_pool", g.samp_data_arr]])
				samp_img_pool_loaded = True
				output.put(["nn", ["save_to_folder", save_path]])
		
		if g.button_dic.get('regen_texture') != None and g.button_dic['regen_texture'].pressed():
			output.put(["nn", ["regen_texture", g.samp_data_arr[g.samp_selected], g.samp_selected, g.slider_dic["resol_sld"].value]])
		
		if g.button_dic.get('add_samp') != None and g.button_dic['add_samp'].pressed():
			from fn_1 import SampData
			samp_img_pool_loaded = False
			add_samp(None, SampData(0,0, *base_samp_wh, None, None, 0, None, 1, 1))
		
		if g.button_dic.get('delete') != None and g.button_dic['delete'].pressed():
			del_samp()
			samp_img_pool_loaded = False
			
		if g.button_dic.get('set_comb_wh') != None and g.button_dic['set_comb_wh'].pressed():
			wh = (g.slider_dic["comb_w"].value, g.slider_dic["comb_h"].value)
			mask.clear()
			for _ in range(len(g.samp_data_arr)):
				s = blank_surf(wh)
				s.set_alpha(85)
				mask.append(s)
				mask[-1].fill(COLOR.BLACK)
			comb_img = blank_surf(wh)
			output.put(["nn", ["new_mask", wh]])
		
		if g.button_dic.get('reset_tensor') != None and g.button_dic['reset_tensor'].pressed():
			output.put(["nn", ["reset_tensor"]])
		
		if g.toggle_dic.get('iterate') != None and g.toggle_dic['iterate'].changed:
			output.put(["nn", ["iterate", g.toggle_dic['iterate'].on]])
		
		if g.toggle_dic.get('learn') != None and g.toggle_dic['learn'].changed:
			output.put(["nn", ["learn", g.toggle_dic['learn'].on]])
		
		if g.toggle_dic.get('pool') != None and g.toggle_dic['pool'].changed:
			output.put(["nn", ["pool", g.toggle_dic['pool'].on]])
				
		if g.toggle_dic.get('mix') != None and g.toggle_dic['mix'].changed:
			output.put(["nn", ["mix", g.toggle_dic['mix'].on]])
		
		g.left_clicked = False
		g.right_clicked = False
		prev_xy = g.mouse_xy
		complete_inputs()
		
	win_quit()
	output.put(["done"])

def init_win_thread(menu_width, log_width, graph_height):
	from multiprocessing import Queue, Process
	from multiprocessing import freeze_support as py_install_suport
	py_install_suport()
	input = Queue()
	set_log_queue(input)
	output = Queue()
	win_thread = Process(target=mainn, args=(menu_width, log_width, graph_height, input, output), daemon=False)
	win_thread.start()
	return input, output, win_thread
