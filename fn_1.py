circle_cache = {}

def surf_box(w, h):
	from pygame import Surface, Rect, SRCALPHA
	from pygame.draw import rect as draw
	box = Surface((w, h), SRCALPHA)
	draw(box, (66,66,66), Rect(0, 0, w-1, h-1), 2)
	return box

def surf_from_floats(tensor):
	from fn_0 import float_to_byte
	from pygame.pixelcopy import make_surface
	return make_surface(float_to_byte(tensor))

def rect_point(xy):
	from pygame import Rect as rect
	return rect(*xy, 1, 1)

def draw_box(x, y, w, h, *argv):
	from numpy import ones
	from globals import g
	from pygame import Surface, SRCALPHA
	from pygame.pixelcopy import make_surface
	from pygame.transform import scale as scale_surf
	if len(argv[0]) > 3:
		if len(argv) > 2:
			surf = make_surface(ones((256,1,3), dtype='u1')*255)
		else:
			surf = Surface((256, 1), SRCALPHA)
	else:
		if len(argv) > 2:
			surf = make_surface(ones((256,1,3), dtype='u1')*255)
		else:
			surf = Surface((256, 1))
	g.window.blit(scale_surf(surf, (w, h)), (x, y))


def load_img(name, x=64, y=32):
	from pygame.image import load as load_surf
	from pygame.transform import smoothscale as smoothscale_surf
	from fn_0 import mei_path
	return smoothscale_surf(load_surf(mei_path('assets/'+name+'.png')), (x, y))

def gen_circle(outer_radius, iner_radius, invert=False):
	from numpy import ndarray
	from globals import COLOR
	from pygame.color import Color
	from numpy.linalg import norm as dist
	from globals import g
	global circle_cache
	target = g.slider_dic["brush_target"].value
	c = circle_cache.get((outer_radius, iner_radius, target, invert))
	if str(c.__class__) == "<class 'NoneType'>":
		circle_cache.clear()
	else:
		return c.copy()
	neg = ndarray([outer_radius*2+1, outer_radius*2+1, 4], dtype="u1")
	pos = neg.copy()
	if outer_radius-iner_radius > 0:
		gradiant_num = 255/(outer_radius-iner_radius)
	for x in range(neg.shape[0]):
		for y in range(neg.shape[1]):
			d = dist([outer_radius-x, outer_radius-y])
			if d > outer_radius:
				neg[x,y] = Color(COLOR.NONE)
				pos[x,y] = Color(COLOR.NONE)
			elif d <= iner_radius:
				neg[x,y] = Color(COLOR.MASK_BASE)
				pos[x,y] = (target,target,target, Color(COLOR.MASK)[3])
			else:
				v = gradiant_num*(outer_radius-d)
				neg[x,y] = (0,0,0, Color(COLOR.MASK_BASE)[3]*v//255)
				pos[x,y] = (target,target,target, Color(COLOR.MASK)[3]*v//255)
	circle_cache[(outer_radius, iner_radius, target, True)] = neg
	circle_cache[(outer_radius, iner_radius, target, False)] = pos
	if invert:
		return neg
	else:
		return pos

def set_pixel(pos, mask):
	from time import time_ns
	from pygame.image import fromstring as bytes_to_surf
	from globals import g
	w, h = mask[g.samp_selected].get_size()
	if pos[0] < 0 or pos[1] < 0 or pos[0] >= w or pos[1] >= h: return

	outer_radius = (g.slider_dic["brush_size"].value-1)//2
	iner_radius = g.slider_dic["brush_hardness"].value*outer_radius
	circle_arr = gen_circle(outer_radius, iner_radius)

	circle_surf = bytes_to_surf(circle_arr.tobytes(), circle_arr.shape[:2], "RGBA")
	mask[g.samp_selected].blit(circle_surf, (pos[0]-outer_radius, pos[1]-outer_radius))
	g.send_samp_dic[g.samp_selected] = time_ns()+1e8

def del_pixel(pos, mask):
	from time import time_ns
	from pygame.image import fromstring as bytes_to_surf
	from globals import g
	w, h = mask[g.samp_selected].get_size()
	if pos[0] < 0 or pos[1] < 0 or pos[0] >= w or pos[1] >= h: return

	outer_radius = (g.slider_dic["brush_size"].value-1)//2
	iner_radius = g.slider_dic["brush_hardness"].value*outer_radius
	circle_arr = gen_circle(outer_radius, iner_radius, True)

	circle_surf = bytes_to_surf(circle_arr.tobytes(), circle_arr.shape[:2], "RGBA")
	mask[g.samp_selected].blit(circle_surf, (pos[0]-outer_radius, pos[1]-outer_radius))
	g.send_samp_dic[g.samp_selected] = time_ns()+1e8

def draw_surf(surf, x, y, size=1, area=None):
	if surf == None: return
	from fn_0 import vec
	from pygame import Rect as rect
	from pygame import Surface, SRCALPHA
	from pygame.transform import scale as scale_surf
	from globals import g, COLOR
	w = surf.get_width()
	h = surf.get_height()
	if size != 1:
		w = int(w*size)
		h = int(h*size)
		x *= size
		y *= size
	if area == None:
		if x < 0:
			if x > -1: x = 0
			x = g.window.get_size()[0]+x-w
		if y < 0:
			if y > -1: y = 0
			y = g.window.get_size()[1]+y-h
		area = rect(0, 0, w+x, h+y)
	pos = vec(x,y)
	if size == 1:
		g.window.blit(surf, (area.x, area.y), rect(-x, -y, area.width, area.height))
	elif w+h > area.width+area.height:
		crop_wh = vec(area.width//size, area.height//size)+2
		rem_x, rem_y = vec(size-x%size, size-y%size)
		cropped = Surface(crop_wh, SRCALPHA)

		alpha = surf.get_alpha()
		if str(alpha.__class__) != "<class 'NoneType'>" and alpha < 255:
			surf.set_alpha(255)
			cropped.set_alpha(alpha)
			cropped.blit(surf, pos//size+1)
			surf.set_alpha(alpha)
		else:
			cropped.blit(surf, pos//size+1)

		r = rect(rem_x, rem_y, area.width, area.height)
		xy = (int(crop_wh[0]*size), int(crop_wh[1]*size))
		g.window.blit(scale_surf(cropped, xy), area.topleft, r)
	elif size > 1:
		g.window.blit(scale_surf(surf, (w, h)), (area.x, area.y), rect(-x, -y, area.width, area.height))
	elif size < 1:
		g.window.blit(scale_surf(surf, (w, h)), (area.x, area.y), rect(-x, -y, area.width, area.height))

def draw_text_box(x, y, w, h, text, selected):
	from time import time_ns
	from pygame import Rect as rect
	from pygame.draw import line as draw_line_on_surf
	from globals import g, font, COLOR
	filed_surf = surf_box(w, h)
	textobj = font.txt.render(str(text), True, COLOR.WHITE)
	textrect = textobj.get_rect()
	textrect.topleft = (x+7, y+4)
	g.window.blit(textobj, textrect)
	if selected and time_ns()%1e9>5e8:
		draw_line_on_surf(filed_surf, COLOR.WHITE, (textrect.right-5, 4), (textrect.right-5, 25), 1)
	g.window.blit(filed_surf, rect(x, y, w, h))

def draw_bar(x, y, w, bar_w, procent, value):
	from fn_0 import create_surf
	from globals import g, font, COLOR

	g.window.blit(create_surf(bar_w, 24, COLOR.GREYc), (x+4, y+4))
	g.window.blit(create_surf(8, 32, COLOR.Tc_BLACK), (x+procent*bar_w, y))
	g.window.blit(create_surf(w-bar_w-16, 32, COLOR.NONE, 2, COLOR.GREY4), (x+16+bar_w, y))

	textobj = font.txt.render(str(value), True, COLOR.WHITE)
	textrect = textobj.get_rect()
	textrect.topleft = (x+20+bar_w, y+3)
	g.window.blit(textobj, textrect)

def draw_text(text, x, y, w=1e9, h=1e9, center=False, spacing=2):
	from globals import g, font, COLOR
	words = text.split()
	lines = []
	while len(words) > 0:
		line_words = []
		while len(words) > 0:
			line_words.append(words.pop(0))
			fw, fh = font.txt.size(' '.join(line_words + words[:1]))
			if fw > w: break
		line = ' '.join(line_words)
		lines.append(line)
	y_offset = 0
	if x < 0: x += g.win_wh[0]
	if y < 0: y += g.win_wh[1]
	y-=4
	for line in lines:
		fw, fh = font.txt.size(line)
		tx = x - fw / 2
		ty = y + y_offset
		textobj = font.txt.render(line, True, COLOR.WHITE)
		textrect = textobj.get_rect()
		if center: textrect.topleft = (tx, ty)
		else: textrect.topleft = (x, ty)
		g.window.blit(textobj, textrect)
		y_offset += fh-4#+spacing
		if y_offset > h:
			break

def fan_format(x, _):
	if x > 999e3:
		return "{0:.3g}".format(x/1024**2)[:4]+"TiB"
	elif x > 999:
		return "{0:.3g}".format(x/1024)[:4]+"GiB"
	else:
		return "{0:.3g}".format(x)[:4]+"MiB"

class BtnData():
	def __init__(self, x, y, w, h, text):
		from pygame import Rect as rect
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.text = text
		self.rect = rect(x, y, w, h)
		self.on = False

	def pressed(self):
		if self.on:
			self.on = False
			return True

class TogData():
	def __init__(self, x, y, w, h, on):
		from pygame import Rect as rect
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.rect = rect(x,y,w,h)
		self.on = on
		self.changed = False

class SldData():
	def __init__(self, x, y, w, min, max, value, mult, offset, box_w):
		from pygame import Rect as rect
		self.x = x
		self.y = y
		self.w = w
		self.bar_w = w-box_w
		self.rect = rect(x, y, self.bar_w+8, 32)
		self.min = min
		self.max = max
		self.mult = mult
		self.offset = offset
		self.value = value*mult+offset
		if max-min == 0:
			self.frac = 0.5
		else:
			self.frac = (value-min)/(max-min)

class SampData():
	def __init__(self, left, top, width, height, w, h, epoch, loss, scale, weight):
		self.left = left
		self.top = top
		self.width = width
		self.height = height
		self.w = w
		self.h = h
		self.epoch = epoch
		self.loss = loss
		self.scale = scale
		self.weight = weight
